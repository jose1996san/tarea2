package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
    TextView TextViewprecio, TextViewdescripcion, TextViewnombre;
    ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
//  Se recibe los parametros de la actividad detail.xml

        String object = getIntent().getStringExtra("object_id");
        TextViewprecio = (TextView) findViewById(R.id.precio);
        TextViewnombre = (TextView)findViewById(R.id.nombre);
        TextViewdescripcion = (TextView)findViewById(R.id.description);
        imagen = (ImageView)findViewById(R.id.imagen);
        //accede a las propiedades del object name, price, description e image.
        DataQuery query = DataQuery.get("item");
        String parametro = getIntent().getExtras().getString("objeto1");
        //Se recibe el parametro añadido en el Layout activity_detail.xml
        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){
                    //Se recibe a las propiedades de name, price, description e imagen.
                    String precio = (String) object.get("price")+("\u0024");
                    String descripcion = (String) object.get("description");
                    String nombre = (String) object.get("name");
                    Bitmap ImagenBitmap = (Bitmap) object.get("image");
                    //Aqui se guardan los datos
                    TextViewprecio.setText(precio);
                    TextViewdescripcion.setText(descripcion);
                    TextViewnombre.setText(nombre);
                    imagen.setImageBitmap(ImagenBitmap);

                }else {
                    //error
                    // FIN - CODE6
                }
            }
        });
    }}
